﻿
using System;

namespace Bitz.Modules.Core.Audio
{
    public interface IAudioPlayer
    {
        Channels Channel { get; set; }
        Boolean DisposeOnFinish { get; }
        Boolean Looping { get; set; }
        Boolean Paused { get; }
        Single Pitch { get; set; }
        Boolean Playing { get; }
        Single Volume { get; set; }

        void Pause();
        void Play();
        void Resume();
        void Stop(Boolean unRegister = false);
        void Update();
    }
}