﻿using System;
using Bitz.Modules.Core.Foundation.Services;

namespace Bitz.Modules.Core.Audio
{
    public interface IAudioService : IService
    {
        void StopAllSounds();
        void LoadSound(String path);
        void ReserveChannel(Channels channel);
        void UnreserveChannel(Channels channel);

        /// <summary> Returns data for a loaded sound file </summary>
        /// <param name="path">Path of the sound you want</param>
        /// <returns></returns>
        IAudioData GetData(String path);

        IAudioPlayer CreateAudioPlayer(String path, Boolean playOnStart = false, Channels channel = Channels.CHANNEL_ANY, Boolean looping = false, Boolean disposeOnFinish = true);
    }
}