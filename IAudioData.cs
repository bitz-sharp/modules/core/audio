﻿using System;

namespace Bitz.Modules.Core.Audio
{
    public interface IAudioData
    {
        Int32 AudioChannels { get; }
        Int32 BitRate { get; }
        Int32[] BufferIds { get; }
        String FilePath { get; }
        Int32 SampleRate { get; }
        Byte[] SoundData { get; }
    }
}