﻿namespace Bitz.Modules.Core.Audio
{
    /// <summary> Channels for Audio </summary>
    public enum Channels
    {
        CHANNEL_01,
        CHANNEL_02,
        CHANNEL_03,
        CHANNEL_04,
        CHANNEL_05,
        CHANNEL_06,
        CHANNEL_07,
        CHANNEL_08,
        CHANNEL_09,
        CHANNEL_10,
        CHANNEL_11,
        CHANNEL_12,
        CHANNEL_13,
        CHANNEL_14,
        CHANNEL_15,
        CHANNEL_16,
        CHANNEL_ANY
    }
}